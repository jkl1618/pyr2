Example: simple 2D ERI survey with topography

1. Open th GUI (pyR2):
	- Option 1: use standalone version
	- Option 2: use command line: 	cd /pyr2/src
									python ui.py

2. Select "Inverse" (checked by default)
3. Select working directory: /examples/2DTopo-dc
4. Select data type from drop down menu: Syscal (for this example)
5. Import data: syscalFileTopo.csv
6. From "Topography" tab, select "Import from csv (no headers)" (top), then import "elecTopo.csv"
7. Follow the rest of the instructions from pyR2 manual.